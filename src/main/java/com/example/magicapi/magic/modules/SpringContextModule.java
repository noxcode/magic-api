package com.example.magicapi.magic.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.config.MagicModule;

/**
 * @Description:
 * @author: Nox
 * @date: 2021/3/8
 */
@Component
public class SpringContextModule implements MagicModule {

    @Autowired
    private ApplicationContext context;


    @Override
    public String getModuleName() {
        return "spring";
    }

    public Object getBean(String beanName){
        return context.getBean(beanName);
    }
    public <T> T getBean(Class<T> beanType){
        return context.getBean(beanType);
    }
}
