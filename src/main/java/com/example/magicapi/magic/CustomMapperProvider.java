package com.example.magicapi.magic;

import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.provider.impl.CamelColumnMapperProvider;

/**
 * @Description: 自定义列名转换
 * @author: Nox
 * @date: 2021/3/8
 */
@Component
public class CustomMapperProvider extends CamelColumnMapperProvider {

    @Override
    public String name() {
        // 当配置完成后
        // 可以在配置文件中全局配置magic-api.sql-column-case=custom
        // 也可以在代码中写 db.columnCase('custom').select(); 来使用
        return "custom";
    }

    @Override
    public String mapping(String columnName) {
        // 自定义逻辑
        return super.mapping(columnName.toLowerCase());
    }

}
