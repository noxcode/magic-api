package com.example.magicapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.ssssssss.magicapi.adapter.Resource;
import org.ssssssss.magicapi.adapter.resource.DatabaseResource;

/**
 * @Description:
 * @author: Nox
 * @date: 2021/3/8
 */
@Configuration
public class MagicConfig {

    @Bean
    public Resource databaseResource(JdbcTemplate jdbcTemplate){
        /**
         *  magic-api_file 为表名，其表结构为
         *  file_path varchar(512) 主键
         *  file_content mediumtext/clob(mysql/oracle)
         *  0.7以上版本将不再提供建表语句，需自行建表
         */
        return new DatabaseResource(jdbcTemplate, "magic_api_file");
    }
}
